#include "SPI_control.h"

//typedef uint16_t VideoBuff[ROW_VIDEO_BUFF_SIZE][COL_VIDEO_BUFF_SIZE];

static uint32_t volatile spiTransmitPointer;

static uint8_t volatile rowIterator;
static uint32_t volatile colIterator;
static uint8_t volatile numOfRow;
static uint8_t volatile rowStartN;

static buff_t volatile * dataPtr;
static uint32_t volatile dataSize;

static void (*SpiTransmitInterruptRoutinePtr)(void);

//volatile static buff_t (*twoDimensionVideoBuff)[ROW_VIDEO_BUFF_SIZE][COL_VIDEO_BUFF_SIZE];
//volatile static buff_t (*twoDimensionVideoBuff)[COL_VIDEO_BUFF_SIZE];

volatile union
{
	uint8_t volatile OneDimension_8[ROW_VIDEO_BUFF_SIZE * COL_VIDEO_BUFF_SIZE*2];
	uint8_t volatile TwoDimension_8[ROW_VIDEO_BUFF_SIZE][COL_VIDEO_BUFF_SIZE*2];
//	uint32_t volatile HalfDimension[ROW_VIDEO_BUFF_SIZE * COL_VIDEO_BUFF_SIZE / (sizeof(uint32_t)/sizeof(buff_t))];
	buff_t volatile OneDimension[ROW_VIDEO_BUFF_SIZE * COL_VIDEO_BUFF_SIZE];
	buff_t volatile TwoDimension[ROW_VIDEO_BUFF_SIZE][COL_VIDEO_BUFF_SIZE];
} VideoBuff;

void InitSpi1(void)
{
	//GPIO init

	//use GPIOB PB5 (MOSI) and GPIOB PB3 (CLK)
	RCC->AHBENR |= RCC_AHBENR_GPIOBEN;

	//PB5 (MOSI) init
	GPIOB->MODER &= ~GPIO_MODER_MODER5;
	//alternate function
	GPIOB->MODER |= GPIO_MODER_MODER5_1;
	//push-pull
	GPIOB->OTYPER &= ~GPIO_OTYPER_OT_5;
	//speed
	GPIOB->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR5;
	//no pullup pulldown
	GPIOB->PUPDR &= ~GPIO_PUPDR_PUPDR5;

	//Selection of alternate function for PB5 (MOSI)
	GPIOB->AFR[0] &= ~GPIO_AFRL_AFRL5;
	GPIOB->AFR[0] |= (ALTER_FUNC_5 & GPIO_AFRL_AFRL5);

	//PB3 (CLK) init
	GPIOB->MODER &= ~GPIO_MODER_MODER3;
	//alternate function
	GPIOB->MODER |= GPIO_MODER_MODER3_1;
	//push-pull
	GPIOB->OTYPER &= ~GPIO_OTYPER_OT_3;
	//speed
	GPIOB->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR3;
	//no pullup pulldown
	GPIOB->PUPDR &= ~GPIO_PUPDR_PUPDR3;

	//Selection of alternate function for PB3 (CLK)
	GPIOB->AFR[0] &= ~GPIO_AFRL_AFRL3;
	GPIOB->AFR[0] |= (ALTER_FUNC_5 & GPIO_AFRL_AFRL3);

	//SPI1 init

	//enable clock
	RCC->APB2ENR |= RCC_APB2ENR_SPI1EN;

	//spi baud rate = 16MHz / 32
	SPI1->CR1 &= ~SPI_CR1_BR;
	SPI1->CR1 |= SPI_CR1_BR_2 | SPI_CR1_BR_0;

	//16 bit for frame
	SPI1->CR1 |= SPI_CR1_DFF;

	//8 bit for frame
	//SPI1->CR1 &= ~SPI_CR1_DFF;

	//high data first normal:MSB
	SPI1->CR1 &= ~SPI_CR1_LSBFIRST;
//	SPI1->CR1 |= SPI_CR1_LSBFIRST;

	//MASTER
	SPI1->CR1 |= SPI_CR1_MSTR;

	SPI1->CR2 |= SPI_CR2_SSOE;

	//CLK polarity

	//	___|-|_|-|_|-|_ CLK
	//     .   .   .
	//    ###|###|###|  DATA

	SPI1->CR1 &= ~SPI_CR1_CPOL;
	SPI1->CR1 &= ~SPI_CR1_CPHA;

	//enable
	SPI1->CR1 |= SPI_CR1_SPE;

	NVIC_SetPriority (SPI1_IRQn, 1);		//#TODO change USART IRQn from 1 to 2
	NVIC_EnableIRQ (SPI1_IRQn);

	//Tx buffer empty interrupt enable
	//SPI1->CR2 |= SPI_CR2_TXEIE;


	//func clear
	CsEnable = NULL_P;
	CsDisable = NULL_P;
}


/*
ErrorStateEnum SpiStartTransmit(volatile buff_t * ptr, uint32_t size)
{
	if((CsEnable==NULL_P) || (CsDisable==NULL_P)) return ERROR_P;
	CsEnable();
	spiTransmitPointer = 0;
	dataPtr = ptr;
	dataSize = size;
	SpiTransmitInterruptRoutine();
	SPI1->CR2 |= SPI_CR2_TXEIE;

	return SUCCESS_P;
}*/

ErrorStateEnum SpiTransmitCmd(uint16_t cmd)
{
	if((CsEnable==NULL_P) || (CsDisable==NULL_P)) return ERROR_P;
	CsEnable();
	SPI1->DR = cmd;
	SpiTransmitInterruptRoutinePtr = SpiTransmitInterruptRoutineCmd;
	SPI1->CR2 |= SPI_CR2_TXEIE;

	return SUCCESS_P;
}


void SPI1_IRQHandler(void)
{
	//SpiTransmitInterruptRoutine();
	SpiTransmitInterruptRoutinePtr();
}

//void inline SpiTransmitInterruptRoutine(void)
//{
//	if(spiTransmitPointer < dataSize)
//	{
//		SPI1->DR = dataPtr[spiTransmitPointer];
//		spiTransmitPointer++;
//	}
//	else
//	{
//		volatile uint32_t i = 0;
//
//		SPI1->CR2 &= ~SPI_CR2_TXEIE;
//
//		while((SPI1->SR & SPI_SR_BSY) || (i >= SPI_BSY_TIMEOUT)) i++;
//		CsDisable();
//	}
//}

ErrorStateEnum DisplayPrintBuff(volatile buff_t * ptr, uint8_t numberOfRow, uint8_t rowStart)
{
	if((CsEnable==NULL_P) || (CsDisable==NULL_P)) return ERROR_P;
	CsEnable();
//	twoDimensionVideoBuff = (buff_t (*)[COL_VIDEO_BUFF_SIZE])ptr;
	rowIterator = 0;
	colIterator = 0;
	rowStartN = rowStart + 1;
	numOfRow = numberOfRow;
	SpiTransmitInterruptRoutinePtr = SpiTransmitInterruptRoutineBuff;
//	SpiTransmitInterruptRoutine();
	SPI1->DR = WRITE_CMD | ReverseByte(rowStartN);
	SPI1->CR2 |= SPI_CR2_TXEIE;

	return SUCCESS_P;
}

void SpiTransmitInterruptRoutineBuff(void)
{
	if(colIterator < COL_VIDEO_BUFF_SIZE)
	{
		SPI1->DR = VideoBuff.TwoDimension[rowIterator][colIterator];
		colIterator++;
	}
	else
	{
		rowIterator++;
		if(rowIterator < numOfRow)
		{
			colIterator = 0;
			SPI1->DR = ReverseByte(rowStartN + rowIterator);
		}
		else
		{
			volatile uint32_t i = 0;
			//push trailer at the end
			SPI1->DR = TRAILER_CMD;
			//disable interrupt
			SPI1->CR2 &= ~SPI_CR2_TXEIE;

			while((SPI1->SR & SPI_SR_BSY) || (i >= SPI_BSY_TIMEOUT)) i++;

			CsDisable();
		}
	}
}

void SpiTransmitInterruptRoutineCmd(void)
{
	volatile uint32_t i = 0;
	SPI1->CR2 &= ~SPI_CR2_TXEIE;
	while((SPI1->SR & SPI_SR_BSY) || (i >= SPI_BSY_TIMEOUT)) i++;

	CsDisable();
}
