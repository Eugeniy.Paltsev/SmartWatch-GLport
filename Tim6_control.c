#include "Tim6_control.h"

void Tim6Init(void)
{
	RCC->APB1ENR |= RCC_APB1ENR_TIM6EN;
	//clear to reset value
	TIM6->CR2 = 0;
	//set prescaller
	TIM6->PSC = 0x1061;
	//set limit
	TIM6->ARR = 0x773;
	//enable interrupt, disable DMA request
	TIM6->DIER = TIM_DIER_UIE & (~TIM_DIER_UDE);

	if(ReadPortA3()) TIM6->ARR = 0xFE;
	else TIM6->ARR = 0xEE6;

	NVIC_SetPriority (TIM6_IRQn, 2);
	NVIC_EnableIRQ (TIM6_IRQn);

	//enable TIM6
	TIM6->CR1 |= TIM_CR1_CEN;
}

void TIM6_IRQHandler(void)
{
	//clear UIF
	TIM6->SR &= ~TIM_SR_UIF;
	//Process inputs and outputs
	Tim6InterruptRoutine();
}

void Tim6InterruptRoutine(void)
{
	LedChangeState_A0();
}
