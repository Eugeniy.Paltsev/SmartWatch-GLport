#ifndef LED_CONTROL
#define LED_CONTROL

#include "stm32l1xx.h"

/*
 * LED funcs
 */
void InputInit(void);

uint32_t ReadPortA3(void);

void LedInit(void);

void LedOn_A0(void);

void LedOff_A0(void);

void LedOn_A1(void);

void LedOff_A1(void);

void LedOn_A2(void);

void LedOff_A2(void);

void inline LedChangeState_A0(void);

void inline LedChangeState_A1(void);

void inline LedChangeState_A2(void);

#endif
